package com.example.faccipmfrancoreyesneivisapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.TextView;



public class DatosDetalles extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datos_detalles);



        TextView userId = findViewById(R.id.mUserId);
        TextView title_li = findViewById(R.id.mTitle);
        TextView body_li = findViewById(R.id.mbody_li);

        Bundle bundle = getIntent().getExtras();
        String mTitle = bundle.getString("title");
        String mBodyView = bundle.getString("body");
        int mUserId = bundle.getInt("userId");


        userId.setText(Double.toString(mUserId));
        title_li.setText(mTitle);
        body_li.setText(mBodyView);

    }
}