package com.example.faccipmfrancoreyesneivisapp;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Singleton {


    private RequestQueue requestQueue;
    private static Singleton mInstance;

    private Singleton(Context context){
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized Singleton getmInstance(Context context){

        if (mInstance == null){
            mInstance = new Singleton(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue(){return requestQueue;}
}
