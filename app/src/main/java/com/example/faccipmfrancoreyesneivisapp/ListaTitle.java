package com.example.faccipmfrancoreyesneivisapp;

public class ListaTitle {
    private String title , body;
    private int userId;

    public ListaTitle(String title , String body , int userId){
        this.title = title;
        this.body = body;
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public int getUserId() {
        return userId;
    }
}

