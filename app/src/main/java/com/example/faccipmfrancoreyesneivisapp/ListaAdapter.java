package com.example.faccipmfrancoreyesneivisapp;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ListaAdapter extends RecyclerView.Adapter<ListaAdapter.TitleHolder>{
    private Context context;
    private List<ListaTitle> titleList;

    public ListaAdapter(Context context , List<ListaTitle> movies){
        this.context = context;
        titleList = movies;
    }
    @NonNull
    @Override
    public TitleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.title , parent , false);
        return new TitleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TitleHolder holder, int position) {

        ListaTitle title = titleList.get(position);
        holder.title.setText(title.getTitle());

        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context , DatosDetalles.class);

                Bundle bundle = new Bundle();
                bundle.putString("title" , title.getTitle());
                bundle.putString("body" , title.getBody());
                bundle.putInt("userId" , title.getUserId());

                intent.putExtras(bundle);

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return titleList.size();
    }

    public class TitleHolder extends RecyclerView.ViewHolder{

        TextView principalView, title;
        ConstraintLayout constraintLayout;

        public TitleHolder(@NonNull View itemView) {
            super(itemView);

            principalView = itemView.findViewById(R.id.principalView);
            title = itemView.findViewById(R.id.title_li);
            constraintLayout = itemView.findViewById(R.id.main_layout);
        }
    }
}


